
Feature('User can plan a trip');

Scenario('planner returns with list of trip option', (I,homePage) => {
    I.amOnPage('/');
    I.say('Entering "From" data');
    I.waitForEnabled(homePage.fields.from);
    I.fillField(homePage.fields.from,'North Sydney Station');

    I.say('Entering "To" data');
    I.waitForEnabled(homePage.fields.to);
    I.fillField(homePage.fields.to,"Town Hall Station");

    I.say('Clicking on Go');
    I.waitForEnabled(homePage.go);
    I.click(homePage.go);

    I.say('Verifying if results are visible and in total 4');
    //Verify number of results available on page
    I.waitNumberOfVisibleElements(homePage.resultList,4,5)
});
