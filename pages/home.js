
const I = actor();

module.exports = {

    fields: {
        from: {css: 'div #search-input-From'},
        to: {css: 'div #search-input-To'}
    },
    go: {css: 'div button#search-button'},
    resultList: {css: '.tripResults.tp-result-item'}
};
