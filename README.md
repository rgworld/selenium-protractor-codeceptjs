# selenium-protractor-codeceptjs

---
##To run test on local machine
 
I have added installation script within `package.json` file.

To install `selenium-standalone` server, follow these steps

1. Run `npm run selenium:install`
2. Run `selenium-standalone start`
on a separate terminal
3. Run `npm test` to start a single `chrome` session and test

Note: Browser binaries should be installed on your system.
